﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProceduralLevel : MonoBehaviour {

    public static String NormalBall = "NormalBall";
    public static String SpikyBall = "SpikyBall";
    public static String SwirlyBall = "SwirlyBall";

    public Transform ball;
    public Transform ballGraphics;

    public Transform goal;
    public Light pointLight;
    
    public Sprite defaultBallSprite;

    public Text uiStrikes;
    public RectTransform uiPanel;
    public RectTransform canvas;
    public Text uiHypeText;

    public Transform elementCorner;
    public Transform elementFloor;
    public Transform elementWall;
    public Transform elementChipped;
    public Transform elementSaddle;
    public Transform elementFilled;
    
    public int width;
    public int height;    
    public double initialWallProbability;
    public int goals;
    public int bufferFillingWidth;
    
    private List<Transform> levelGoals;

    private Transform levelBall;
    private Transform levelBallGraphics;
    private BallControls ballScript;

    protected System.Random random = new System.Random();

    public static int ELEMENT_EMPTY = 0;
    public static int ELEMENT_WALL = 1;

    private Boolean useGoalLights = true;

    void Start () {
        GenerateLevel();
    }

    void Update() {
        Screen.SetResolution(Screen.width, Screen.height, false);

        if (levelGoals.TrueForAll((Transform g) => { return g == null; })) {
            Debug.Log("All goals destroyed, generating new level");
            //foreach (Transform t in levelObjects) Destroy(t.gameObject);
            //GenerateLevel();
            SceneManager.LoadScene("main");
        }
    }

    public void ChangeBall(Sprite ballSprite, String ballName) {
        levelBallGraphics.GetComponent<SpriteRenderer>().sprite = ballSprite;
        if (ballName == NormalBall) {
            ballScript.SetMaxBounces(10);
            ballScript.rotateNaturally = true;
            ballScript.rotateInMotion = false;
            ballScript.rotateInRest = false;
            levelBall.GetComponent<Rigidbody2D>().drag = 0.0f;
            levelBall.GetComponent<Rigidbody2D>().gravityScale = 3;
        }
        else if (ballName == SpikyBall) {
            ballScript.SetMaxBounces(0);
            ballScript.rotateNaturally = false;
            ballScript.rotateInMotion = true;
            ballScript.rotateInRest = false;
            levelBall.GetComponent<Rigidbody2D>().drag = 0.0f;
            levelBall.GetComponent<Rigidbody2D>().gravityScale = 3;
        }
        else if (ballName == SwirlyBall) {
            ballScript.SetMaxBounces(1);
            ballScript.rotateNaturally = false;
            ballScript.rotateInMotion = true;
            ballScript.rotateInRest = true;
            levelBall.GetComponent<Rigidbody2D>().drag = 0.0f;
            levelBall.GetComponent<Rigidbody2D>().gravityScale = 0;
        }
    }

    public void HypeText(String text) {        
        Text t = Instantiate(uiHypeText, new Vector3(0, 0, -0.5f), Quaternion.identity);        
        t.text = text;
        t.GetComponent<RectTransform>().SetParent(canvas, false);

        Debug.Log("New hype text: " + text);
    }
        
    private void GenerateLevel() {
        Debug.Log("Generating level");
        
        levelGoals = new List<Transform>();
        
        int[,] matrix = InitialMatrix();        

        Debug.Log("Level generated, calculating contours");

        int ballI = int.MaxValue;
        int ballJ = int.MinValue;

        for (var i = 0; i < height - 1; i++) {
            for (var j = 0; j < width - 1; j++) {
                var t1 = (matrix[i + 1, j] == ELEMENT_WALL) ? "1" : "0";
                var t2 = (matrix[i + 1, j + 1] == ELEMENT_WALL) ? "1" : "0";
                var t3 = (matrix[i, j + 1] == ELEMENT_WALL) ? "1" : "0";
                var t4 = (matrix[i, j] == ELEMENT_WALL) ? "1" : "0";
                var code = Convert.ToInt32(t4 + t3 + t2 + t1, 2);

                CreateElement(j, height - i, code);

                if (ElementsInNeighborhood(matrix, i, j, height, width, ELEMENT_EMPTY) == 9) {
                    ballI = i;
                    ballJ = j;
                }
            }
        }

        // Create a buffer filling zone around the map
        for (var i = -bufferFillingWidth; i < height + bufferFillingWidth; i++) {
            for (var j = -bufferFillingWidth; j < width + bufferFillingWidth; j++) {
                if (!(j >= 0 && i >= 0 && j < width - 1 && i < height - 1))
                    CreateElement(j, height - i, 15);
            }
        }

        Debug.Log("Contours ready, calculating goal positions");

        List<int[]> goalPositions = CalculateGoalPositions(matrix, ballI, ballJ, goals);

        Debug.Log("Positions ready, spawning ball and goals");

        foreach (int[] goalPosition in goalPositions) {
            Vector3 pos = new Vector3(goalPosition[0], height - goalPosition[1], 0);
            
            levelGoals.Add(Instantiate(goal, pos, Quaternion.identity));

            if (useGoalLights) {
                GameObject goalLight = new GameObject("Goal light");
                Light lightComp = goalLight.AddComponent<Light>();
                lightComp.color = Color.white;
                lightComp.range = 30;
                lightComp.intensity = 10;
                goalLight.transform.position = new Vector3(pos.x, pos.y, -1);
            }
        }

        levelBall = Instantiate(ball, new Vector3(ballJ, height - ballI, 0), Quaternion.identity);
        levelBallGraphics = Instantiate(ballGraphics, Vector3.zero, Quaternion.identity);
        ballScript = levelBall.gameObject.GetComponent<BallControls>();

        ballScript.level = this;
        ballScript.uiStrikes = uiStrikes;
        ballScript.uiPanel = uiPanel;
        levelBallGraphics.parent = levelBall;
        levelBallGraphics.localPosition = Vector3.zero;
        ChangeBall(defaultBallSprite, NormalBall);
        ballScript.graphics = levelBallGraphics;
    }

    private List<int[]> CalculateGoalPositions(int[,] matrix, int ballI, int ballJ, int goals) {
        Dictionary<String, double> distances = new Dictionary<string, double>();

        Debug.Log("Calculating shortest path tree from ball point...");

        Dijkstra(matrix, distances, new HashSet<String>(), ballJ, ballI, 0);

        Debug.Log("Shortest path tree calculated");

        List<int[]> goalPositions = new List<int[]>();

        double minDistanceBetweenGoals = 25;

        List<String> validDistanceKeys = new List<string>();

        foreach (String key in distances.Keys) {
            String[] parts = key.Split(':');
            int x1 = Convert.ToInt32(parts[0]);
            int y1 = Convert.ToInt32(parts[1]);if (ElementsInNeighborhood(matrix, y1, x1, height, width, ELEMENT_EMPTY) == 9) validDistanceKeys.Add(key);
        }

        for (int g = 0; g < goals; g++) {
            String bestKey = "";
            double biggestDistance = 0;
            foreach (String key in validDistanceKeys) {
                String[] parts = key.Split(':');
                int x1 = Convert.ToInt32(parts[0]);
                int y1 = Convert.ToInt32(parts[1]);

                if (distances[key] > biggestDistance && 
                    goalPositions.TrueForAll((int[] p) => { return farEnough(p, x1, y1, minDistanceBetweenGoals);})) {
                    bestKey = key;
                    biggestDistance = distances[key];
                }
            }

            if (bestKey != "") {
                String[] parts = bestKey.Split(':');
                int x1 = Convert.ToInt32(parts[0]);
                int y1 = Convert.ToInt32(parts[1]);
                goalPositions.Add(new int[] { x1, y1 });
                Debug.Log("Added goal position at x: " + x1 + ", y: " + y1);
            }
            else {
                Debug.Log("Could not find eligible goal position for goal #" + g);
            }
        }

        return goalPositions;
    }

    private Boolean farEnough(int[] p, int x1, int y1, double min) {
        return Math.Sqrt(Math.Pow(p[0] - x1, 2) + Math.Pow(p[1] - y1, 2)) > min;
    }

    private void Dijkstra(int[,] matrix, Dictionary<String, double> distances, 
        HashSet<String> visited, int x, int y, double currentDistance) {

        visited.Add(x + ":" + y);
        for (var n = y - 1; n <= y + 1; n++) {
            for (var m = x - 1; m <= x + 1; m++) {
                if (n >= 0 && m >= 0 && n < height && m < width && matrix[n, m] == ELEMENT_EMPTY) {
                    double addDist = Math.Sqrt(Math.Abs(n - y) + Math.Abs(m - x));
                    updateDistance(distances, m, n, currentDistance + addDist);
                }
            }
        }

        String nextKey = "";
        double smallestTentative = int.MaxValue;

        foreach (String key in distances.Keys) {
            if (!visited.Contains(key) && distances[key] < smallestTentative) {
                smallestTentative = distances[key];
                nextKey = key;
            }
        }
        
        if (nextKey != "") {            
            String[] parts = nextKey.Split(':');
            int m = Convert.ToInt32(parts[0]);
            int n = Convert.ToInt32(parts[1]);
            Dijkstra(matrix, distances, visited, m, n, smallestTentative);
        }
    }

    private void updateDistance(Dictionary<String, double> distances, int x, int y, double distance) {
        String key = x + ":" + y;
        if (distances.ContainsKey(key)) {
            if (distances[key] > distance) distances[key] = distance;
        } else {
            distances[key] = distance;
        }
    }



    private Transform CreateElement(int x, int y, int elementMask) {
        Vector3 coords = new Vector3(x, y, 0);
        switch(elementMask)
        {            
            case 1:
                return Instantiate(elementCorner, coords, Quaternion.identity);
            case 2:
                return FlipHorizontal(Instantiate(elementCorner, coords, Quaternion.identity));
            case 3:
                return Instantiate(elementFloor, coords, Quaternion.identity);
            case 4:
                return FlipBoth(Instantiate(elementCorner, coords, Quaternion.identity));
            case 5:
                return Instantiate(elementSaddle, coords, Quaternion.identity);
            case 6:
                return FlipHorizontal(Instantiate(elementWall, coords, Quaternion.identity));
            case 7:
                return FlipHorizontal(Instantiate(elementChipped, coords, Quaternion.identity));
            case 8:
                return FlipVertical(Instantiate(elementCorner, coords, Quaternion.identity));
            case 9:
                return Instantiate(elementWall, coords, Quaternion.identity);
            case 10:
                return FlipHorizontal(Instantiate(elementSaddle, coords, Quaternion.identity));
            case 11:
                return Instantiate(elementChipped, coords, Quaternion.identity);
            case 12:
                return FlipVertical(Instantiate(elementFloor, coords, Quaternion.identity));
            case 13:
                return FlipVertical(Instantiate(elementChipped, coords, Quaternion.identity));
            case 14:
                return FlipBoth(Instantiate(elementChipped, coords, Quaternion.identity));
            case 15:
                return Instantiate(elementFilled, coords, Quaternion.identity);
            default:
                return null;
        }
    }

    private Transform FlipHorizontal(Transform o) {
        o.transform.localScale = new Vector3(-1, 1, 1);
        return o;
    }

    private Transform FlipVertical(Transform o) {
        o.transform.localScale = new Vector3(1, -1, 1);
        return o;
    }

    private Transform FlipBoth(Transform o) {
        o.transform.localScale = new Vector3(-1, -1, 1);
        return o;
    }

    private int[,] InitialMatrix() {
        //int levelType = random.Next(2);
        int levelType = 2;
        LevelGen levelGen;
        if (levelType == 0) {
            levelGen = new RegularLevelGen(height, width, initialWallProbability);
        }
        else if (levelType == 1) {
            levelGen = new FreeWaysLevelGen(height, width, initialWallProbability);
        }
        else {
            levelGen = new PlatformsLevelGen(height, width, initialWallProbability);
        }

        return levelGen.Generate();
    }

    public static int ElementsInNeighborhood(int[,] matrix, int i, int j, int height, int width, int element) {
        int c = 0;

        for (var n = i - 1; n <= i + 1; n++) {
            for (var m = j - 1; m <= j + 1; m++) {
                if (n >= 0 && m >= 0 && n < height && m < width && matrix[n, m] == element) ++c;
            }
        }
        return c;
    }
}

public abstract class LevelGen {
    protected System.Random random = new System.Random();

    protected readonly int height;
    protected readonly int width;
    protected readonly double wallP;

    public LevelGen(int height, int width, double wallP) {
        this.height = height;
        this.width = width;
        this.wallP = wallP;
    }

    public abstract int[,] Generate();

    protected virtual int[,] Next(int[,] matrix) {
        int[,] next = new int[matrix.GetLength(0), matrix.GetLength(1)];

        for (var i = 0; i < height; i++) {
            for (var j = 0; j < width; j++) {
                int iin = Nearby(matrix, i, j, ProceduralLevel.ELEMENT_WALL);
                if (i == 0 || j == 0 || i == height - 1 || j == width - 1 || iin >= 5) next[i, j] = ProceduralLevel.ELEMENT_WALL;
                else next[i, j] = ProceduralLevel.ELEMENT_EMPTY;
            }
        }
        return next;
    }

    protected int Nearby(int[,] matrix, int i, int j, int element) {
        return ProceduralLevel.ElementsInNeighborhood(matrix, i, j, height, width, element);
    }
    
}

class RegularLevelGen : LevelGen {
    public RegularLevelGen(int height, int width, double wallP) : base(height, width, wallP) {}

    override public int[,] Generate() {
        int[,] matrix = new int[height, width];

        for (var i = 0; i < height; i++) {
            for (var j = 0; j < width; j++) {
                if (random.NextDouble() < wallP) matrix[i, j] = ProceduralLevel.ELEMENT_WALL;
                else matrix[i, j] = ProceduralLevel.ELEMENT_EMPTY;
            }
        }

        for (int i = 0; i < 5; i++)
            matrix = Next(matrix);

        return matrix;
    }
}

class FreeWaysLevelGen : LevelGen {
    private int hway1;
    private int hway2;
    private int vway1;

    public FreeWaysLevelGen(int height, int width, double wallP) : base(height, width, wallP) {
        hway1 = random.Next(2, height / 2);
        hway2 = random.Next(height / 2, height - 1);
        vway1 = random.Next(width);
    }

    override public int[,] Generate() {
        int[,] matrix = new int[height, width];
        
        for (var i = 0; i < height; i++) {
            for (var j = 0; j < width; j++) {
                if (random.NextDouble() < wallP && !Freeway(i, j)) matrix[i, j] = ProceduralLevel.ELEMENT_WALL;
                else matrix[i, j] = ProceduralLevel.ELEMENT_EMPTY;
            }
        }

        for (int i = 0; i < 4; i++)
            matrix = Next(matrix);

        return matrix;
    }

    private Boolean Freeway(int i, int j) {
        return !((Math.Abs(i - hway1) > 1) && (Math.Abs(i - hway2) > 1) && (Math.Abs(j - vway1) > 1));
    }
}

class PlatformsLevelGen : LevelGen {

    public PlatformsLevelGen(int height, int width, double wallP) : base(height, width, wallP) { }

    override public int[,] Generate() {
        int[,] matrix = new int[height, width];
                
        for (var i = 0; i < height; i++) {
            for (var j = 0; j < width; j++) {
                if (i == 0 || j == 0 || i == height - 1 || j == width - 1) matrix[i, j] = ProceduralLevel.ELEMENT_WALL;
                else matrix[i, j] = ProceduralLevel.ELEMENT_EMPTY;
            }
        }

        int platforms = random.Next(5, 7);

        for (var i = 0; i < platforms; i++) {
            AddPlatform(matrix, i, platforms);
        }

        return matrix;
    }

    private void AddPlatform(int[,] matrix, int n, int total) {        
        int altitude = height / total * n + (random.Next(5)-2);

        int holeStart = random.Next(width - 10);
        int holeWidth = random.Next(4, 8);

        //for (var i = altitude; i < altitude + thickness; i++) {
            for (var j = 0; j < width; j++) {
                if (j < holeStart || j > holeStart + holeWidth)
                    matrix[altitude, j] = ProceduralLevel.ELEMENT_WALL;
            }
        //}
    }
}