﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;
using UnityEngine.UI;

public class BallControls : MonoBehaviour {
    
    public Text uiStrikes;
    public RectTransform uiPanel;

    public int maxRemainingBounces = 2;
    public int remainingBounces = 0;

    public Transform graphics;
    public Boolean rotateNaturally = false;
    public Boolean rotateInMotion = false;
    public Boolean rotateInRest = false;

    public Shader aimTrailShader;

    public ProceduralLevel level;
    
    private Rigidbody2D ball;    
    private LineRenderer aimTrail;
    private GameObject ballLight;
    private int strikes = 0;

    private Boolean mouseDown = false;
    private Vector3 mouseDownPosition = Vector3.zero;

    private Boolean useBallLight = false;
    
    private static int BOUNCE_GRACE_PERIOD = 100;

    private Stopwatch bounceGrace = new Stopwatch();

    // Use this for initialization
    void Start () {
        ball = GetComponent<Rigidbody2D>();
        
        aimTrail = GetComponent<LineRenderer>();
        aimTrail.material = new Material(Shader.Find("Particles/Additive"));

        bounceGrace.Start();

        if (useBallLight) {
            ballLight = new GameObject("Goal light");
            Light lightComp = ballLight.AddComponent<Light>();
            lightComp.color = Color.white;
            lightComp.range = 30;
            lightComp.intensity = 10;

            ballLight.transform.position = new Vector3(ball.position.x, ball.position.y, -1);
        }
    }
    
    // Update is called once per frame
    void Update() {
        if (ballLight != null)
            ballLight.transform.position = new Vector3(ball.position.x, ball.position.y, -1);

        if (mouseDown) {
            //UpdateAimTrail();
        }

        if (Input.GetMouseButtonDown(0) && !MouseInUI()) {
            // click
            Debug.Log("mouse pressed");
            mouseDown = true;
            //aimTrail.numPositions = 2;
            //UpdateAimTrail();
            mouseDownPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0) && mouseDown) {
            // release
            Vector3 delta = (Input.mousePosition - mouseDownPosition);
            Debug.Log("mouse released with force " + Math.Sqrt(Math.Pow(delta.x, 2) + Math.Pow(delta.y, 2)));
            bounceGrace.Reset();
            bounceGrace.Start();
            mouseDown = false;
            //aimTrail.numPositions = 0;
            float power = delta.magnitude;
            Vector3 direction = delta.normalized;

            updateUiValue(uiStrikes, ++strikes);
            ball.isKinematic = false;            
            ball.velocity = Vector3.zero;            
            ball.AddForce(delta, ForceMode2D.Impulse);
        }

        if (rotateNaturally)
            graphics.localEulerAngles = ball.transform.localEulerAngles;
        else if (ball.velocity == Vector2.zero && rotateInRest)
            graphics.Rotate(0, 0, Time.deltaTime * 700);
        else if (ball.velocity != Vector2.zero && rotateInMotion)
            graphics.Rotate(0, 0, Time.deltaTime * 700);
    }

    public void OnGUI() {
        if (mouseDown) {
            PlotTrajectory(ball.position, (Input.mousePosition - mouseDownPosition)/ball.mass, 0.05f, 3f);
        }
    }

    private Vector3 PlotTrajectoryAtTime(Vector3 pos, Vector3 velocity, float time) {
        return pos + velocity * time + (Physics.gravity*ball.gravityScale) * time * time * 0.5f;
    }

    private void PlotTrajectory(Vector3 start, Vector3 startVelocity, float timestep, float maxTime) {
        Vector3 prev = start;
        for (float t = timestep; t < maxTime ; t += timestep) {
            if (t > maxTime) break;
            Vector3 pos = PlotTrajectoryAtTime(start, startVelocity, t);
            if (Physics2D.Linecast(prev, pos)) break;

            Vector2 guiBallPos = Camera.main.WorldToScreenPoint(pos);
            GUIDrawRect(new Rect(new Vector2(guiBallPos.x, Screen.height - guiBallPos.y), new Vector2(5, 5)), Color.white);
            //Debug.DrawLine(prev, pos, Color.red);
            prev = pos;
        }
    }

    private static Texture2D _staticRectTexture;
    private static GUIStyle _staticRectStyle;

    // Note that this function is only meant to be called from OnGUI() functions.
    public static void GUIDrawRect(Rect position, Color color) {
        if (_staticRectTexture == null) {
            _staticRectTexture = new Texture2D(1, 1);
        }

        if (_staticRectStyle == null) {
            _staticRectStyle = new GUIStyle();
        }

        _staticRectTexture.SetPixel(0, 0, color);
        _staticRectTexture.Apply();

        _staticRectStyle.normal.background = _staticRectTexture;

        GUI.Box(position, GUIContent.none, _staticRectStyle);


    }

    private Boolean MouseInUI() {
        return RectTransformUtility.RectangleContainsScreenPoint(
            uiPanel,
            Input.mousePosition,
            Camera.main);
    }

    void UpdateAimTrail() {
        aimTrail.SetPosition(0, new Vector3(ball.position.x, ball.position.y, -5));

        Vector2 newPos = ball.position + ((Vector2)(Input.mousePosition - mouseDownPosition) / 30);

        aimTrail.SetPosition(1, new Vector3(newPos.x, newPos.y, -5));
    }

    public void SetMaxBounces(int b) {
        maxRemainingBounces = b;
        remainingBounces = b;
    }
    
    private void OnCollisionEnter2D(Collision2D collision) {
        if (bounceGrace.ElapsedMilliseconds > BOUNCE_GRACE_PERIOD) {
            bounceGrace.Reset();
            bounceGrace.Start();
            Debug.Log("ball collided with remaining bounces: " + remainingBounces);
            if (remainingBounces == 0) {
                level.HypeText("!");
                remainingBounces = maxRemainingBounces;
                ball.velocity = Vector3.zero;
                ball.angularVelocity = 0;
                ball.isKinematic = true;
            }
            else {
                level.HypeText(remainingBounces.ToString());
                --remainingBounces;                
            }
        } else {
            Debug.Log("ball collision ignored due to grace period");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log("picked up " + collision.gameObject.name);
        Destroy(collision.gameObject);
    }
    
    private void updateUiValue(Text text, int g) {
        text.text = text.text.Remove(text.text.IndexOf(':')) + ": " + g;
    }
}
