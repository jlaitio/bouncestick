﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using UnityEngine.UI;

public class HypeTextBehaviour : MonoBehaviour {

    public int duration = 2000;
    public int maxScale = 5;

    private Transform t;
    private Text text;

    private Stopwatch lifeTime = new Stopwatch();

	void Start() {        
        lifeTime.Start();
        t = GetComponent<Transform>();
        text = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update() {
        long life = lifeTime.ElapsedMilliseconds;

        if (life > duration) {
            Debug.Log("Destroying hype text");
            Object.Destroy(this.gameObject);
        }
        else {
            float lifeRatio = ((float) life) / duration;
            t.localScale = new Vector3(lifeRatio * maxScale, lifeRatio * maxScale, 1);
            text.color = new Color(1.0f, 1.0f, 1.0f, 1 - lifeRatio);
        }
    }
}
