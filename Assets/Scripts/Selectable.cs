﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Selectable : MonoBehaviour, ISelectHandler {
    public ProceduralLevel level;
    
    public void OnSelect(BaseEventData eventData) {        
        Image img = GetComponent<Image>();

        Debug.Log("ball type '" + img.name + "' selected");

        level.ChangeBall(img.sprite, img.name);
    }
}
