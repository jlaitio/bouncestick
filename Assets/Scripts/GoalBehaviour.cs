﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalBehaviour : MonoBehaviour {

    Transform goal;

	void Start () {
        goal = GetComponent<Transform>();
	}
	
	void Update () {
        goal.Rotate(0, 0, Time.deltaTime * 100);
        goal.Rotate(0, Time.deltaTime * 100, 0, Space.World);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log("goal noticed it was eaten");
    }
}
