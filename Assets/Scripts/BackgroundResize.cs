﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundResize : MonoBehaviour {

	void Start () {
        Transform transform = GetComponent<Transform>();

        Vector2 spriteSize = GetComponent<SpriteRenderer>().sprite.rect.size;
        Vector2 localSpriteSize = spriteSize / GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Vector3 worldSize = localSpriteSize;
        worldSize.x *= transform.lossyScale.x;
        worldSize.y *= transform.lossyScale.y;

        Vector3 screenSize = 0.5f * worldSize / Camera.main.orthographicSize;
        screenSize.y *= Camera.main.aspect;

        Vector3 inPixels = new Vector3(screenSize.x * Camera.main.pixelWidth, screenSize.y * Camera.main.pixelHeight, 0) * 0.5f;

        transform.localScale = new Vector2(Camera.main.pixelWidth / inPixels.x, Camera.main.pixelHeight / inPixels.y);
	}
}
